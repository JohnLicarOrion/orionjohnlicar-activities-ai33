var ctx = document.getElementById("barChart").getContext("2d");
var myChart = new Chart(ctx, {
  type: "bar",
  data: {
    labels: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
    ],
    datasets: [
      {
        label: "Returned",

        backgroundColor: "#75BBC8",
        hoverBackgroundColor: "green",
        data: [12, 19, 3, 5, 2, 3, 5, 2],
      },
      {
        label: "Burrowed",

        backgroundColor: "#16556F",
        hoverBackgroundColor: "red",
        data: [15, 26, 2, 6, 8, 6, 3, 3],
      },
    ],
  },
  options: {
    legend: {
      position: "top",
      align: "start",
      labels: {
        boxWidth: 15,
        fontSize: 15,
      },
    },
    tooltips: { mode: "index" },
    scales: {
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
          gridLines: {
            display: false,
          },
        },
      ],
    },
  },
});

//  doughnut chart
var ctx = document.getElementById("pieChart").getContext("2d");
var myChart = new Chart(ctx, {
  type: "doughnut",
  data: {
    labels: ["Fiction", "Mystery", "Thriller", "Horror"],
    datasets: [
      {
        label: "Number of Return",

        backgroundColor: ["#8899AA", "#16556F", "#FFD662", "#75BBC8"],
        hoverBackgroundColor: "green",
        data: [50, 50, 50, 50],
      },
    ],
  },
  options: {
    legend: {
      position: "bottom",
      align: "center",
      labels: {
        boxWidth: 15,
        fontSize: 15,
      },
    },
  },
});
