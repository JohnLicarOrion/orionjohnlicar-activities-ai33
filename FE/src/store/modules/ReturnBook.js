import ReturnedBooksBookBaseURI from "../../config/Returned_Book";

const state = {
  Returned_Books: [],
};

const getters = {
  AllReturnedBook: (state) => state.Returned_Books,
};

const actions = {
  async fetchReturnedBooks({ commit }) {
    commit("LoadingStatus", true);
    const response = await ReturnedBooksBookBaseURI.index();
    commit("LoadingStatus", false);
    commit("ReturnedBook", response.data);
  },

  async AddReturnedBooks({ commit, dispatch }, ReturnedBook) {
    const response = await ReturnedBooksBookBaseURI.store(ReturnedBook);
    commit("AddReturnedBook", response.data.data);
    dispatch("fetchBooks");
    dispatch("fetchBorrowedBook");
  },
};

const mutations = {
  ReturnedBook: (state, ReturnedBook) => (state.Returned_Books = ReturnedBook),
  AddReturnedBook: (state, ReturnedBook) => {
    state.Returned_Books.unshift(ReturnedBook);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
