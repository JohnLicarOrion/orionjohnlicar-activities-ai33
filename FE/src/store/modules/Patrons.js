import PatronBaseURI from "../../config/Patron";

const state = {
  Patrons: [],
  BorrowerBook: [],
};

const getters = {
  AllPatrons: (state) => state.Patrons,
};

const actions = {
  async fetchPatrons({ commit }) {
    try {
      commit("LoadingStatus", true);
      const response = await PatronBaseURI.index();
      commit("LoadingStatus", false);
      commit("Patrons", response.data);
    } catch (errors) {
      this._vm.$toast.error("Netwrok Error", {
        timeout: 10000,
      });
    }
  },

  async AddOnePatron({ commit }, Patron) {
    const response = await PatronBaseURI.store(Patron);
    commit("AddPatron", response.data.data);
  },
  async UpdateOnePatron({ commit }, Patron) {
    const response = await PatronBaseURI.update(Patron);
    commit("UpdatePatron", response.data.data);
  },
  async RemoveOnePatron({ commit }, Patron) {
    PatronBaseURI.destroy(Patron);
    commit("RemovePatron", Patron.id);
  },
};

const mutations = {
  Patrons: (state, Patrons) => (state.Patrons = Patrons),
  AddPatron: (state, Patron) => {
    state.Patrons.unshift(Patron);
  },
  UpdatePatron: (state, Patron) => {
    const index = state.Patrons.findIndex((item) => item.id === Patron.id);
    if (index !== -1) state.Patrons.splice(index, 1, { ...Patron });
  },
  RemovePatron: (state, Patron) => {
    state.Patrons = state.Patrons.filter((item) => {
      return item.id != Patron;
    });
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
