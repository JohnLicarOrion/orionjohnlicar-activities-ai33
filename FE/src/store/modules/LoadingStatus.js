const state = {
  LoadingStatus: false,
};

const getters = {
  LoadingStatus: (state) => state.LoadingStatus,
};

const mutations = {
  LoadingStatus: (state, newLoadingStatus) => {
    state.LoadingStatus = newLoadingStatus;
  },
};
export default {
  state,
  getters,
  mutations,
};
