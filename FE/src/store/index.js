import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import BorrowedBook from "./modules/BorrowBook";
import Category from "./modules/Category";
import LoadingStatus from "./modules/LoadingStatus";
import Patrons from "./modules/Patrons";
import ReturnedBook from "./modules/ReturnBook";

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [
    Books,
    Category,
    Patrons,
    LoadingStatus,
    BorrowedBook,
    ReturnedBook,
  ],
});
