import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../pages/dashboard.vue";
import Patron from "../pages/patron.vue";
import Book from "../pages/book.vue";
import Settings from "../pages/setting.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  linkActiveClass: "SideBar-active",
  routes: [
    {
      path: "/",

      component: Dashboard,
      name: "Dashboard",
    },

    {
      path: "/Book",
      name: "Book",
      component: Book,
    },
    {
      path: "/Patron",
      name: "Patron",
      component: Patron,
    },
    {
      path: "/Settings",
      name: "Settings",
      component: Settings,
    },
  ],
});
