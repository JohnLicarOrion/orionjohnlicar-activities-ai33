import Api from "./Api";
const Base = "Borrowed_Book";
export default {
  index() {
    return Api.get(Base);
  },
  store(Borrowed_Book) {
    return Api.post(Base, Borrowed_Book);
  },
  update(Borrowed_Book) {
    return Api.put(`${Base}/${Borrowed_Book.id}`, Borrowed_Book);
  },
  destroy(Borrowed_Book) {
    return Api.delete(`${Base}/${Borrowed_Book.id}`);
  },
};
