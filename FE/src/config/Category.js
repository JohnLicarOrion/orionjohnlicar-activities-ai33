import Api from "./Api";
const Base = "Categories";
export default {
  index() {
    return Api.get(Base);
  },
  store(Category) {
    return Api.post(Base, Category);
  },
  update(Category) {
    return Api.put(`${Base}/${Category.id}`, Category);
  },
  destroy(Category) {
    return Api.delete(`${Base}/${Category.id}`);
  },
};
