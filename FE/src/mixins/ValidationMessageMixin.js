export const ValidationMessageMixin = {
  methods: {
    PatronValidation() {
      if (!this.$v.PatronForm.first_name.required) {
        this.errorMessage.push("First Name is required");
      }

      if (!this.$v.PatronForm.first_name.minLength)
        this.errorMessage.push(
          "First Name must have at least have " +
            this.$v.PatronForm.first_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronForm.middle_name.required)
        this.errorMessage.push("Middle Name is required");

      if (!this.$v.PatronForm.middle_name.minLength)
        this.errorMessage.push(
          "Middle Name must have at least have " +
            this.$v.PatronForm.middle_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronForm.last_name.required)
        this.errorMessage.push("Last Name is required");

      if (!this.$v.PatronForm.last_name.minLength)
        this.errorMessage.push(
          "Last Name must have at least have " +
            this.$v.PatronForm.last_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronForm.email.required)
        this.errorMessage.push("Email is required");

      if (!this.$v.PatronForm.email.email)
        this.errorMessage.push("Email is must be valid");

      if (!this.$v.PatronForm.email.minLength)
        this.errorMessage.push(
          "Email must have at least have " +
            this.$v.PatronForm.email.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronForm.email.uniqueEmail)
        this.errorMessage.push("Email is already taken");
      return this.errorMessage.length ? true : false;
    },
    PatronForUpdateValidation() {
      if (!this.$v.PatronTobeUpdated.first_name.required) {
        this.errorMessage.push("First Name is required");
      }

      if (!this.$v.PatronTobeUpdated.first_name.minLength)
        this.errorMessage.push(
          "First Name must have at least have " +
            this.$v.PatronForm.first_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.middle_name.required)
        this.errorMessage.push("Middle Name is required");

      if (!this.$v.PatronTobeUpdated.middle_name.minLength)
        this.errorMessage.push(
          "Middle Name must have at least have " +
            this.$v.PatronForm.middle_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.last_name.required)
        this.errorMessage.push("Last Name is required");

      if (!this.$v.PatronTobeUpdated.last_name.minLength)
        this.errorMessage.push(
          "Last Name must have at least have " +
            this.$v.PatronForm.last_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.email.required)
        this.errorMessage.push("Email is required");

      if (!this.$v.PatronTobeUpdated.email.email)
        this.errorMessage.push("Email is must be valid");

      if (!this.$v.PatronTobeUpdated.email.minLength)
        this.errorMessage.push(
          "Email must have at least have " +
            this.$v.PatronForm.email.$params.minLength.min +
            " character"
        );
      return this.errorMessage.length ? true : false;
    },
    BookValidation() {
      if (!this.$v.BookForm.name.required) {
        this.errorMessage.push("Book Name is required");
      }

      if (!this.$v.BookForm.name.minLength)
        this.errorMessage.push(
          "Book Name must have at least have " +
            this.$v.BookForm.name.$params.minLength.min +
            " character"
        );

      if (!this.$v.BookForm.name.uniqueBookName)
        this.errorMessage.push("Book Name is already taken");

      if (!this.$v.BookForm.author.required)
        this.errorMessage.push("Author is required");

      if (!this.$v.BookForm.author.minLength)
        this.errorMessage.push(
          "Author must have at least have " +
            this.$v.BookForm.author.$params.minLength.min +
            " character"
        );

      if (!this.$v.BookForm.copies.minValue)
        this.errorMessage.push("Copies is required");
      if (!this.$v.BookForm.copies.numeric)
        this.errorMessage.push("Copies must be Positive numbers");

      if (!this.$v.BookForm.category_id.required)
        this.errorMessage.push("Category is required");

      return this.errorMessage.length ? true : false;
    },
    BookUpdateValidation() {
      if (!this.$v.BookTobeUpdated.name.required) {
        this.errorMessage.push("Book Name is required");
      }

      if (!this.$v.BookTobeUpdated.name.minLength)
        this.errorMessage.push(
          "Book Name must have at least have " +
            this.$v.BookTobeUpdated.name.$params.minLength.min +
            " character"
        );

      if (!this.$v.BookTobeUpdated.author.required)
        this.errorMessage.push("Author is required");

      if (!this.$v.BookTobeUpdated.author.minLength)
        this.errorMessage.push(
          "Author must have at least have " +
            this.$v.BookTobeUpdated.author.$params.minLength.min +
            " character"
        );

      if (!this.$v.BookTobeUpdated.copies.required)
        this.errorMessage.push("Copies is required");
      if (!this.$v.BookTobeUpdated.copies.minValue)
        this.errorMessage.push("Copies is required");
      if (!this.$v.BookTobeUpdated.copies.numeric)
        this.errorMessage.push("Copies must be Positive numbers");

      if (!this.$v.BookTobeUpdated.category_id.required)
        this.errorMessage.push("Category is required");

      return this.errorMessage.length ? true : false;
    },

    BorrowReturnMessage() {
      if (!this.$v.BorrowReturnForm.copies.required)
        this.errorMessage.push("Copies is required");
      if (!this.$v.BorrowReturnForm.copies.minValue)
        this.errorMessage.push("Copies is required");
      if (!this.$v.BorrowReturnForm.copies.numeric)
        this.errorMessage.push("Copies must be Positive numbers");
      if (!this.$v.BorrowReturnForm.patron_id.required)
        this.errorMessage.push("Patron is required");
      if (!this.$v.BorrowReturnForm.book_id.required)
        this.errorMessage.push("Book is required");
      return this.errorMessage.length ? true : false;
    },
  },
};
