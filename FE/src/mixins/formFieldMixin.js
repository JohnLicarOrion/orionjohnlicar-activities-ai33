export const formFieldMixin = {
  data() {
    return {
      Search: "",
      Selected: "",
    };
  },
  methods: {
    HideModal() {
      this.$refs.modal.hide();
      this.FormReset();
    },
    FormReset() {
      (this.PatronForm = {
        last_name: "",
        first_name: "",
        middle_name: "",
        email: "",
      }),
        (this.PatronTobeUpdated = {
          last_name: "",
          first_name: "",
          middle_name: "",
          email: "",
        }),
        (this.BookForm = {
          name: "",
          author: "",
          copies: 0,
          category_id: "",
        }),
        (this.BookTobeUpdated = {
          name: "",
          author: "",
          copies: 0,
          category_id: "",
        }),
        (this.BorrowReturnForm = {
          patron_id: "",
          copies: 0,
          book_id: "",
        });
      this.Selected = "";
      this.PatronInfo = [];
    },

    highlightMatches(text) {
      const matchExists = text
        .toLowerCase()
        .includes(this.Search.toLowerCase());
      if (!matchExists) return text;

      const re = new RegExp(this.Search, "ig");
      return text.replace(
        re,
        (matchedText) => `<strong>${matchedText}</strong>`
      );
    },

    BorrowerPatrons(arr) {
      if (arr == this.AllPatrons) {
        arr.forEach((patron) => {
          this.PatronInfo.unshift({
            label: `${patron.first_name} ${patron.middle_name} ${patron.last_name}`,
            id: patron.id,
          });
        });
      } else if (arr == this.AllBorrowedBook) {
        arr.forEach((borrower) => {
          if (this.Selected.id == borrower.book_id) {
            this.PatronInfo.unshift({
              label: `${borrower.patron.first_name} ${borrower.patron.middle_name} ${borrower.patron.last_name}`,
              id: borrower.patron_id,
            });
          }
        });
      }
    },
    PatronValidation() {
      if (
        !this.$v.PatronForm.first_name.required ||
        !this.$v.PatronTobeUpdated.first_name.required
      ) {
        this.errorMessage.push("First Name is required");
      }

      if (
        !this.$v.PatronForm.first_name.minLength ||
        !this.$v.PatronTobeUpdated.first_name.minLength
      )
        this.errorMessage.push(
          "First Name must have at least have " +
            this.$v.PatronForm.first_name.$params.minLength.min +
            " character"
        );

      if (
        !this.$v.PatronForm.middle_name.required ||
        !this.$v.PatronTobeUpdated.middle_name.required
      )
        this.errorMessage.push("Middle Name is required");

      if (
        !this.$v.PatronForm.middle_name.minLength ||
        !this.$v.PatronTobeUpdated.middle_name.minLength
      )
        this.errorMessage.push(
          "Middle Name must have at least have " +
            this.$v.PatronForm.middle_name.$params.minLength.min +
            " character"
        );

      if (
        !this.$v.PatronForm.last_name.required ||
        !this.$v.PatronTobeUpdated.last_name.required
      )
        this.errorMessage.push("Last Name is required");

      if (
        !this.$v.PatronForm.last_name.minLength ||
        !this.$v.PatronTobeUpdated.last_name.minLength
      )
        this.errorMessage.push(
          "Last Name must have at least have " +
            this.$v.PatronForm.last_name.$params.minLength.min +
            " character"
        );

      if (
        !this.$v.PatronForm.email.required ||
        !this.$v.PatronTobeUpdated.email.required
      )
        this.errorMessage.push("Email is required");

      if (
        !this.$v.PatronForm.email.email ||
        !this.$v.PatronTobeUpdated.email.email
      )
        this.errorMessage.push("Email is must be valid");

      if (
        !this.$v.PatronForm.email.minLength ||
        !this.$v.PatronTobeUpdated.email.minLength
      )
        this.errorMessage.push(
          "Email must have at least have " +
            this.$v.PatronForm.email.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronForm.email.uniqueEmail)
        this.errorMessage.push("Email is already taken");
      return this.errorMessage.length ? true : false;
    },
    PatronForUpdateValidation() {
      if (!this.$v.PatronTobeUpdated.first_name.required) {
        this.errorMessage.push("First Name is required");
      }

      if (!this.$v.PatronTobeUpdated.first_name.minLength)
        this.errorMessage.push(
          "First Name must have at least have " +
            this.$v.PatronForm.first_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.middle_name.required)
        this.errorMessage.push("Middle Name is required");

      if (!this.$v.PatronTobeUpdated.middle_name.minLength)
        this.errorMessage.push(
          "Middle Name must have at least have " +
            this.$v.PatronForm.middle_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.last_name.required)
        this.errorMessage.push("Last Name is required");

      if (!this.$v.PatronTobeUpdated.last_name.minLength)
        this.errorMessage.push(
          "Last Name must have at least have " +
            this.$v.PatronForm.last_name.$params.minLength.min +
            " character"
        );

      if (!this.$v.PatronTobeUpdated.email.required)
        this.errorMessage.push("Email is required");

      if (!this.$v.PatronTobeUpdated.email.email)
        this.errorMessage.push("Email is must be valid");

      if (!this.$v.PatronTobeUpdated.email.minLength)
        this.errorMessage.push(
          "Email must have at least have " +
            this.$v.PatronForm.email.$params.minLength.min +
            " character"
        );
      return this.errorMessage.length ? true : false;
    },
    BookValidation() {
      if (!this.$v.BookForm.name.required) {
        this.errorMessage.push("Book Name is required");
      }

      if (!this.$v.BookForm.name.minLength)
        this.errorMessage.push(
          "Book Name must have at least have " +
            this.$v.BookForm.name.$params.minLength.min +
            " character"
        );

      if (!this.$v.BookForm.name.uniqueBookName)
        this.errorMessage.push("Book Name is already taken");

      if (!this.$v.BookForm.author.required)
        this.errorMessage.push("Author is required");

      if (!this.$v.BookForm.author.minLength)
        this.errorMessage.push(
          "Author must have at least have " +
            this.$v.BookForm.author.$params.minLength.min +
            " character"
        );

      return this.errorMessage.length ? true : false;
    },
  },
};
