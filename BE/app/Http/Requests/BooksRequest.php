<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          
            'name' => ['required', Rule::unique('Books')->ignore($this->id),' min:2', 'max:255'],
            'author' => ['required',' min:2', 'max:255'],
            'copies' => ['required' ,'min:1', 'integer'],
            'category_id' => ['required', 'exists:Categories,id'],
        ];
    }

    public  function messages()
    {
        return [
            
            'name.required' => 'Name of book is required',
            'name.unique' => 'Name of book already exist',
            'author.required' => 'Author of book is required',
            'copies.required' => 'Copies of book is required',
            'copies.integer' => 'Number of copies must be integer',
            'category_id.required' => 'A book must belong to a category', 
            'category_id.exists' => 'Category ID does not exist exist', 
        ];
    }
}
