<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrowed_Books extends Model
{
    use HasFactory;
    protected $table = 'borrowed_books';
    protected  $fillable = ['patron_id', 'copies', 'book_id'];

    public function book()
    {
        return $this->belongsTo(Books::class, 'book_id');
    }
    public function patron()
    {
        return $this->belongsTo(Patrons::class, 'patron_id');
    }
}
