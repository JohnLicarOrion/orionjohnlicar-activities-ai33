<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Categories::class, 'category_id');
    }

    public function borrowed()
    {
        return $this->hasMany(Borrowed_Books::class, 'book_id');
    }
    public function returned()
    {
        return $this->hasMany(Returned_Books::class, 'book_id');
    }
}
