<?php

use App\Http\Controllers\BooksController;
use App\Http\Controllers\BorrowBookController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::resource('Books', BooksController::class);
Route::resource('Patrons', PatronController::class);
Route::resource('Categories', CategoriesController::class);
Route::resource('Borrowed_Book', BorrowBookController::class);
Route::resource('Returned_Books', ReturnedBookController::class);


